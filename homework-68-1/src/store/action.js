import axios from '../axios-counter';
export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';
export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';

const saveCounter = (value) => {
    return (dispatch, getState) => {
        dispatch(fetchCounterRequest());
        axios.put('/counter.json', {counter: getState().counter + value}).then(response => {
            dispatch(fetchCounterSuccess(response.data.counter));
        }, error => {
            dispatch(fetchCounterError());
        })
    }
};

export const increase = () => {
    return saveCounter(1);
};

export const decrease = () => {
    return saveCounter(-1);
};

export const add = () => {
    return saveCounter(5);
};

export const subtract = () => {
    return saveCounter(-5);
};

export const fetchCounterRequest = () => {
    return { type: FETCH_COUNTER_REQUEST };
};

export const fetchCounterSuccess = (counter) => {
    return { type: FETCH_COUNTER_SUCCESS, counter: counter};
};

export const fetchCounterError = () => {
    return { type: FETCH_COUNTER_ERROR };
};

export const fetchCounter = () => {
    return dispatch => {
        dispatch(fetchCounterRequest());
        axios.get('/counter.json').then(response => {
            dispatch(fetchCounterSuccess(response.data.counter));
        }, error => {
            dispatch(fetchCounterError());
        });
    }
};

