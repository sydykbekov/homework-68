import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import './Counter.css';
import {add, decrease, fetchCounter, increase, subtract} from "../../store/action";
import preloader from '../../assets/Preloader.gif';

class Counter extends Component {
    componentDidMount() {
        this.props.fetchCounter();
    }

    render() {
        return (
            <Fragment>
                <img style={{display: this.props.loading ? 'block' : 'none'}} src={preloader} alt="preloader"/>
                <div className="Counter">
                    <h1>{this.props.ctr}</h1>
                    <button onClick={this.props.increaseCounter}>Increase</button>
                    <button onClick={this.props.decreaseCounter}>Decrease</button>
                    <button onClick={this.props.addCounter}>Increase by 5</button>
                    <button onClick={this.props.subtractCounter}>Decrease by 5</button>
                </div>
            </Fragment>

        )
    }
}

const mapStateToProps = state => {
    return {
        ctr: state.counter,
        loading: state.loading
    };
};

const mapDispatchToProps = dispatch => {
    return {
        increaseCounter: () => dispatch(increase()),
        decreaseCounter: () => dispatch(decrease()),
        addCounter: () => dispatch(add(5)),
        subtractCounter: () => dispatch(subtract(5)),
        fetchCounter: () => dispatch(fetchCounter())
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(Counter);


