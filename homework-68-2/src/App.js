import React, {Component} from 'react';
import ToDoApp from './containers/ToDoApp/ToDoApp';

class App extends Component {
    render() {
        return (
            <ToDoApp />
        );
    }
}

export default App;
