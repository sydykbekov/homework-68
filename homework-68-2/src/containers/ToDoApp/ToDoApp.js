import React, {Component, Fragment} from 'react';
import './ToDoApp.css';
import {connect} from "react-redux";
import {getPosts, removePost, sendPosts} from "../../store/action";
import preloader from '../../assets/Preloader.gif';

class ToDoApp extends Component {
    componentDidMount() {
        this.props.getPosts();
    }

    render() {
        return (
            <Fragment>
                <img style={{display: this.props.loading ? 'block' : 'none'}} src={preloader} alt="preloader"/>
                <div className="container">
                    <input type="text" onChange={this.props.handleChange}/>
                    <button onClick={this.props.sendPosts}>Add</button>
                    <div className="posts">
                        {this.props.posts.map((item, key) => <div className="post" key={key}>{item.value}
                            <span onClick={() => this.props.removePost(item.id)}>delete</span></div>)}
                    </div>
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        posts: state.posts,
        post: state.post,
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        handleChange: event => dispatch({type: 'CHANGE', value: event.target.value}),
        sendPosts: () => dispatch(sendPosts()),
        getPosts: () => dispatch(getPosts()),
        removePost: (id) => dispatch(removePost(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoApp);