import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from 'axios';

import {createStore, applyMiddleware} from 'redux';
import reducer from './store/reducer';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';

const store = createStore(reducer, applyMiddleware(thunkMiddleware));

axios.defaults.baseURL = 'https://list-b6c3c.firebaseio.com/';

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
