import {FETCH_POSTS_REQUEST, FETCH_POSTS_SUCCESS} from "./action";

const initialState = {
    posts: [],
    post: '',
    loading: false
};

const reducer = (state = initialState, action) => {
    if (action.type === 'CHANGE') {
        return {
            ...state,
            post: action.value
        }
    }

    if (action.type === FETCH_POSTS_REQUEST) {
        return {...state, loading: true}
    }

    if (action.type === FETCH_POSTS_SUCCESS) {
        return {...state, posts: action.posts, loading: false};
    }
    return state;
};

export default reducer;