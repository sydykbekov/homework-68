import axios from 'axios';
export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_ERROR = 'FETCH_POSTS_ERROR';
export const FETCH_POSTS_REQUEST = 'FETCH_POSTS_REQUEST';

export const fetchPostsSuccess = (posts) => {
    return {type: FETCH_POSTS_SUCCESS, posts: posts};
};

export const fetchPostsError = () => {
    return {type: FETCH_POSTS_ERROR};
};

export const fetchPostsRequest = () => {
    return {type: FETCH_POSTS_REQUEST};
};

const fetchingPosts = (dispatch) => {
    axios.get('posts.json').then(response => {
        const posts = [];
        for (let key in response.data) {
            posts.unshift({...response.data[key], id: key});
        }
        dispatch(fetchPostsSuccess(posts));
    }, error => {
        dispatch(fetchPostsError());
    });
};

export const sendPosts = () => {
    return (dispatch, getState) => {
        dispatch(fetchPostsRequest());
        axios.post('posts.json', {value: getState().post}).then(() => {
            fetchingPosts(dispatch);
        });
    };
};

export const getPosts = () => {
    return dispatch => {
        dispatch(fetchPostsRequest());
        fetchingPosts(dispatch);
    }
};

export const removePost = (id) => {
    return dispatch => {
        dispatch(fetchPostsRequest());
        axios.delete(`posts/${id}.json`).then(() => {
            fetchingPosts(dispatch);
        });
    }
};